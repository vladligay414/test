import org.testng.annotations.ITestAnnotation;
import sun.jvm.hotspot.utilities.Assert;

import java.util.List;
import java.util.concurrent.TimeUnit;


public class PDPVladTests {

        WebDriver driver;

        @BeforeMethod

        private void beforeMethodSetup() {
            driver = new ChromeDriver();
            driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
            driver.manage().window().maximize();
        }

        @AfterMethod
        private void afterMethodSetup() {
            driver.quit();
        }

//    @Test
//    public void ViewMoreButtonIsDisplayed() {
//
//        driver.get("https://deens-master.now.sh/trips/new-york-state-of-mind-in-new-york_5be35f3644144764acaf29a4");
//        driver.findElement(By.cssSelector("[href*=\"/services/peter-luger\"]")).click();
//
//        WebElement viewMoreButton = driver.findElement(By.cssSelector("button[class='Button__Btn-sc-1ur0jcs-1 kvPtuI']"));
//        Assert.assertTrue(viewMoreButton.isDisplayed());
//    }

        @ITestAnnotation
        public void PictureOfTheFoodPlaceIsDisplayed()
        {
            driver.get("https://deens-master.now.sh/trips/new-york-state-of-mind-in-new-york_5be35f3644144764acaf29a4");
            driver.findElement(By.cssSelector("a[href*='peter']")).click();

            WebElement picture = driver.findElement(By.cssSelector(".service-img"));
            Assert.assertTrue(picture.isDisplayed());
        }

//    @Test
//    public void ViewMoreButtonIsClickable() {
//
//        driver.get("https://deens-master.now.sh/trips/new-york-state-of-mind-in-new-york_5be35f3644144764acaf29a4");
//        driver.findElement(By.cssSelector("a[href*='peter']")).click();
//
//        WebElement viewMoreButton = driver.findElement(By.cssSelector("button[class='Button__Btn-sc-1ur0jcs-1 kvPtuI']"));
//        Assert.assertTrue(viewMoreButton.isEnabled());
//    }

        @Test
        public void VerifyNameOfTheFoodPlace(){
            driver.get("https://deens-master.now.sh/trips/new-york-state-of-mind-in-new-york_5be35f3644144764acaf29a4");
            String foodPlace = driver.findElement(By.cssSelector("a[href*='artisanal']")).getText();
            driver.findElement(By.cssSelector("a[href*='artisanal']")).click();

            String expectedText = "Artisanal Fromagerie & Bistro";
            Assert.assertEquals(foodPlace, expectedText);
        }

        @Test
        public void VerifyInformationSectionIsPresent(){
            driver.get("https://deens-master.now.sh/trips/new-york-state-of-mind-in-new-york_5be35f3644144764acaf29a4");
            driver.findElement(By.cssSelector("a[href*='artisanal']")).click();

            WebElement informationSection = driver.findElement(By.cssSelector("table[class*='Service']"));
            Assert.assertTrue(informationSection.isDisplayed());
        }

        @Test
        public void VerifyThatThereAre5Reviews(){

            driver.get("https://deens-master.now.sh/trips/new-york-state-of-mind-in-new-york_5be35f3644144764acaf29a4");
            driver.findElement(By.cssSelector("a[href*=\"/services/peter\"]")).click();

            List listOfReviews;
            listOfReviews = driver.findElements(By.cssSelector("div[class*='Reviews__Body']"));
            Assert.assertEquals(listOfReviews.size(), 5);
        }

        @Test
        public void VerifyThatThereAre10ReviewsAfterClickingReviewButton(){
            driver.get("https://deens-master.now.sh/trips/new-york-state-of-mind-in-new-york_5be35f3644144764acaf29a4");
            driver.findElement(By.cssSelector("[href*=\"/services/peter-luger\"]")).click();
            driver.findElement(By.cssSelector("[class*=\"Button__Btn\"]")).click();

            WebDriverWait wait = new WebDriverWait(driver, 10);
            wait.until(ExpectedConditions.numberOfElementsToBe(By.cssSelector("[class*=\"Reviews__Body\"]"), 10));

            List listOf10Reviews;
            listOf10Reviews = driver.findElements(By.cssSelector("[class*=\"Reviews__Body\"]"));
            Assert.assertEquals(listOf10Reviews.size(), 10);
        }

    }

}
