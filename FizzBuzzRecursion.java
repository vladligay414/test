public class FizzBuzzRecursion {
    public static void main(String[] args){
        Recur(1);

    }
    private static void Recur(int x) {
        if(x>100)
            return;
        if(x%15==0){
            System.out.println("FizzBuzz");
        }else if(x%3==0) {
            System.out.println("Fizz");
        }else if (x%5==0) {
            System.out.println("Buzz");
        }else
            System.out.println(x);

        Recur(x+1);

    }
}
