import java.util.Scanner;

public class EvenOrNotEven {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the String");
        String str = scan.nextLine();
        if (str.length() % 2 == 0) {
            System.out.println("String is Even");
        } else {
            System.out.println("String is not Even");
        }

    }
}