import java.util.Scanner;

public class Palindromecheck {
       public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the String");
        String originalstring = scan.nextLine();
        String reverse = "";
        originalstring = originalstring.toLowerCase();

        for(int i = originalstring.length() - 1; i >= 0; --i) {
            reverse = reverse + originalstring.charAt(i);
        }

        if (reverse.equals(originalstring)) {
            System.out.println(originalstring + " is a Palindrome");
        } else {
            System.out.println(originalstring + " is not a Palindrome");
        }

    }
}